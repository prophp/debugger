<?php

class DebuggerConfig
{
    private bool $handleErrorsExceptions = false;
    private bool $handleWarnings = false;


    /**
     * @return bool
     */
    public function isHandleErrorsExceptions(): bool
    {
        return $this->handleErrorsExceptions;
    }

    /**
     * @param bool $handleExceptions
     */
    public function setHandleErrorsExceptions(bool $handleExceptions): void
    {
        $this->handleErrorsExceptions = $handleExceptions;
    }

    /**
     * @return bool
     */
    public function isHandleWarnings(): bool
    {
        return $this->handleWarnings;
    }

    /**
     * @param bool $handleWarnings
     */
    public function setHandleWarnings(bool $handleWarnings): void
    {
        $this->handleWarnings = $handleWarnings;
    }

}

class DebuggerConfigFactory
{
    private static function getInstance(): DebuggerConfig
    {
        if (array_key_exists('Debugger', $GLOBALS)) {
            return $GLOBALS['Debugger'];
        }

        return $GLOBALS['Debugger'] = new DebuggerConfig();
    }

    static function handleErrorsExceptions(bool $bool = true): self
    {
        self::getInstance()->setHandleErrorsExceptions($bool);
        return new static;
    }

    static function isHandlingErrorsExceptions(): bool
    {
        return self::getInstance()->isHandleErrorsExceptions();
    }

    static function handleWarnings(bool $bool = true): self
    {
        self::getInstance()->setHandleWarnings($bool);
        return new static;
    }

    static function isHandlingWarnings(): bool
    {
        return self::getInstance()->isHandleWarnings();
    }
}

class Debugger
{
    static public function config(): DebuggerConfigFactory
    {
        return new DebuggerConfigFactory();
    }
}

/*
Debugger::config()
    ::handleErrors()
    ::handleExceptions(false)
    ::handleWarnings();

var_dump(Debugger::config()::isHandlingErrors());
var_dump(Debugger::config()::isHandlingExceptions());
*/